#ifndef TREE_H
#define TREE_H
#include <QList>
#include <QPushButton>
#include <QWidget>
#include <QSignalMapper>
#include <QUrl>
#include <vector>
#include <the_button.h>
#include <stdlib.h>



QT_BEGIN_NAMESPACE
namespace  Ui{class Tree;}
QT_END_NAMESPACE

class Tree : public QWidget
{
    Q_OBJECT

public:
    Tree(std::vector<TheButtonInfo> info);
    Ui::Tree *ui;
    QList<QPushButton*> buttons;
    std::vector<QUrl> urls;



signals:
    void clicked(QUrl url);
    void sender(QUrl url);


private:
    QSignalMapper *signalMapper;
    // maybe add a center function
};

#endif // TREE_H
