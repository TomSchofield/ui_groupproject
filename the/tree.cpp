#include "tree.h"
#include "ui_tree.h"
#include <string>

Tree::Tree(std::vector<TheButtonInfo> info)
{
//    signalMapper = new QSignalMapper(this);
    ui = new Ui::Tree;
    ui->setupUi(this);
    buttons = this->findChildren<QPushButton*>();

    int randNum;
    QUrl current;
    QString name;
    QString style;
    QPushButton* currnetButton;
    for (int i = 0;i < buttons.size() ; i++) {
        name = buttons.at(i)->objectName();
        style = "#" +name+ ":focus{\n background-color: #3cbaa2;\n}\n";
        buttons.at(i)->setStyleSheet(style);
        randNum = rand() % (info.size()-1);
        urls.push_back(*info.at(randNum).url);
        current = *info.at(randNum).url;
        currnetButton = buttons.at(i);
        connect(currnetButton, &QPushButton::clicked, [this, current] { clicked(current); });
    }
}





