#include "treeholder.h"
#include "ui_treeHolder.h"
#include "ui_tree.h"


TreeHolder::TreeHolder(std::vector<TheButtonInfo> info)
{
    ui = new Ui::TreeHolder;
    ui->setupUi(this);
    previewVideoWidget = new QVideoWidget;
    previewVideoWidget->setMinimumSize(200,200);
    previewVideoWidget->setMaximumSize(200,200);
    previewPlayer = new QMediaPlayer;
    previewPlayer->setVideoOutput(previewVideoWidget);
    play = new QPushButton(this);
    play->setText("Play");
    play->setMinimumSize(100,25);
    play->setStyleSheet("""QPushButton:pressed {\n    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,"
                        "   stop:0 rgba(60, 186, 162, 255)"
                        ", stop:1 rgba(98, 211, 162, 255))\n}\nQPushButton "
                        "{\n     background-color: #3cbaa2; border: 1px solid black;\n     "
                        "border-radius: 2px;\n}\n""");
    ui->ButtonsLayout->addWidget(previewVideoWidget);
    ui->ButtonsLayout->addStretch();
    ui->ButtonsLayout->addWidget(play);
    ui->ButtonsLayout->addStretch();

    tree = new Tree(info);
    connect(tree, SIGNAL(clicked(QUrl)), this, SLOT(playVid(QUrl)));
    ui->MapScroll->setWidget(tree);
    ui->MapScroll->verticalScrollBar()->setValue(200);
    ui->MapScroll->horizontalScrollBar()->setValue(410);


//    int t = tree->buttons.indexOf(tree->ui->pushButton_10);
//    QString s = QString(t);
//    play->setText(s);

    //tree->move(500,500);
}

void TreeHolder::playVid(QUrl url){
    CurrentVid = url;
    previewPlayer->setMedia(url);
    previewPlayer->play();

}



