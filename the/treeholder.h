#ifndef TREEHOLDER_H
#define TREEHOLDER_H
#include <QWidget>
#include <QPushButton>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include <QScrollBar>
#include "tree.h"
#include <QUrl>
#include <the_button.h>



QT_BEGIN_NAMESPACE
namespace  Ui{class TreeHolder;}
QT_END_NAMESPACE

class TreeHolder : public QWidget
{
    Q_OBJECT

public:
    TreeHolder(std::vector<TheButtonInfo> info);
    Ui::TreeHolder *ui;
    QVideoWidget *previewVideoWidget;
    QMediaPlayer *previewPlayer;
    QPushButton *play;
    Tree *tree;
    QUrl CurrentVid;
    std::vector<QUrl> infos;

private slots:
    void playVid(QUrl url);

};

#endif // TREEHOLDER_H
