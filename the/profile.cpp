#include "profile.h"
#include "ui_profile.h"

Profile::Profile(QString s, QString p)
{
    username = s;
    path = p;
    ui = new Ui::Profile;
    ui->setupUi(this);
    ui->TopText->setText(username);
    ui->ImageLable->setPixmap(QPixmap(p));
}

void Profile::addTheButton(TheButton* p){
    TheButtons.push_back(p);
    ui->VidLayout->addWidget(p);
}
