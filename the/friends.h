#ifndef FRIENDS_H
#define FRIENDS_H
#include <QWidget>

#include "profilecard.h"


QT_BEGIN_NAMESPACE
namespace  Ui{class Friends;}
QT_END_NAMESPACE


class Friends : public QWidget
{
public:
    Friends();
    Ui::Friends *ui;
    std::vector<ProfileCard*> ProfileCards;
    void addProfileCards(ProfileCard* p);

private:
//    Ui::Friends *ui;

};

#endif // FRIENDS_H
