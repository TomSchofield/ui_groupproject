#include "profilecard.h"
#include "ui_profileCard.h"

ProfileCard::ProfileCard(QWidget *parent, QString s, QString p) :  QWidget(parent)
{
    username = s;
    path = p;
    //QPixmap pix(p);
    ui = new Ui::ProfileCard;
    ui->setupUi(this);
    ui->Username->setText(username);
    ui->ImageLable->setPixmap(QPixmap(p));
    ui->Username->connect(ui->Username, &QPushButton::clicked, this, &ProfileCard::sender);

}


void ProfileCard::sender(){
    emit signalUserName(username);
}
