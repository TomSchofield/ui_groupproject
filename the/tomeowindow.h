#ifndef TOMEOWINDOW_H
#define TOMEOWINDOW_H

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <string>
#include <vector>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QMainWindow>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include <QScrollArea>
#include "friends.h"
#include "profile.h"
#include "profilecard.h"
#include "treeholder.h"


QT_BEGIN_NAMESPACE
namespace  Ui{class MainWindow;}
QT_END_NAMESPACE

class tomeoWindow : public QMainWindow
{
    Q_OBJECT
public:
    tomeoWindow(std::vector<TheButtonInfo> videos);
    ThePlayer *player;
private:
    std::vector<TheButton*> buttons;
    std::vector<TheButton*> TabButtons;
    QLayout* buttonLayout;
    std::vector<TheButtonInfo> videos;
    std::vector<Profile*> Profiles;

    Friends *friendsW;
    QVideoWidget *videoWidget;
    TreeHolder *treeHolder;


    Ui::MainWindow *ui;

    void fillLocationTabs();
    void fillActivityTabs();
    void makeFriends();
    void makeTreeWidget();


private slots:
    void toQInt64(int);
    void toInteger(qint64);
    void setMax(qint64);
    void OpenFriends();
    void openProfile(QString s);
    void openTreeHolder();
    void PlayTreeVid();
    void removeFromTimeline();
    void addToTimeline();





signals:
    void setIntPosition(int);
    void setQInt64Position(qint64);

};

#endif // TOMEOWINDOW_H
