//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include "ui_mainwindow.h"

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i, Ui::MainWindow *ui) {
    buttons = b;
    infos = i;
    this->ui = ui;
    //jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
//    TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
//    buttons -> at( updateCount++ % buttons->size() ) -> init( i );  // seg faults for some reason
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    play();
    currentlyPlaying = button;
    counter =0;
    if(ui){
        ui->addTimeline->show();
        ui->removeTimeline->show();
        ui->playPause->show();
        ui->horizontalSlider->show();
        ui->playPause->setText("Pause");
        friendsW->hide();
        treeHolder->hide();
        videoWidget->show();

        for (int i = 0; i < int(Profiles.size()); i++) {
            Profiles.at(i)->hide();
        }
    }


}

void ThePlayer::playPause(){    //bool x = this->PausedState; //allways returns 1
    if(counter % 2 == 0){
        pause();
        counter++;
        ui->playPause->setText("Play");

    }
    else{
        play();
        counter = 0;
         ui->playPause->setText("Pause");
    }

}

TheButtonInfo* ThePlayer::getCurrent() {
    return currentlyPlaying;
}
