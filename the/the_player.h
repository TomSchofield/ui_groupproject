//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include "ui_mainwindow.h"
#include  "friends.h"
#include  "profile.h"
#include "treeholder.h"

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    std::vector<TheButtonInfo>* infos;
    std::vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    Ui::MainWindow *ui;



public:
    ThePlayer() : QMediaPlayer(NULL) {
        setVolume(0); // be slightly less annoying
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );

        mTimer = new QTimer(NULL);
        mTimer->setInterval(1000); // 1000ms is one second between ...
        //mTimer->setInterval(10000);
        mTimer->start();
        connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
    }

    // all buttons have been setup, store pointers here
    void setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i,Ui::MainWindow *ui);
    std::vector<Profile*> Profiles;
    Friends *friendsW;
    QVideoWidget *videoWidget;
    int counter = 0;
    TreeHolder *treeHolder;
    TheButtonInfo* currentlyPlaying;
    TheButtonInfo* getCurrent();


private slots:

    // change the image and video for one button every one second
    void shuffle();

    void playStateChanged (QMediaPlayer::State ms);

public slots:

    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);

    void playPause();

};

#endif //CW2_THE_PLAYER_H
