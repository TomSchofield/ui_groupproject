#ifndef PROFILE_H
#define PROFILE_H
#include <QWidget>
#include "the_button.h"

QT_BEGIN_NAMESPACE
namespace  Ui{class Profile;}
QT_END_NAMESPACE

class Profile : public QWidget
{
public:
    Profile(QString s,QString path);
    std::vector<TheButton*> TheButtons;
    Ui::Profile *ui;
    QString username;
    QString path;
    void addTheButton(TheButton* p);

};

#endif // PROFILE_H
;
