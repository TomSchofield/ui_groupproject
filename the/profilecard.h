#ifndef PROFILECARD_H
#define PROFILECARD_H
#include <QWidget>


QT_BEGIN_NAMESPACE
namespace  Ui{class ProfileCard;}
QT_END_NAMESPACE

class ProfileCard : public QWidget
{
    Q_OBJECT

public:
    ProfileCard(QWidget *parent,QString s,QString path);
    Ui::ProfileCard *ui;
    QString username;
    QString path;

private slots:
    void sender();

signals:
    void signalUserName(QString);
};

#endif // PROFILECARD_H
