QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        friends.cpp \
        profile.cpp \
        profilecard.cpp \
        the_button.cpp \
        the_player.cpp \
        tomeo.cpp \
        tomeowindow.cpp \
        tree.cpp \
        treeholder.cpp

HEADERS += \
    friends.h \
    profile.h \
    profilecard.h \
    the_button.h \
    the_player.h \
    tomeowindow.h \
    tree.h \
    treeholder.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

FORMS += \
    friends.ui \
    mainwindow.ui \
    profile.ui \
    profileCard.ui \
    tree.ui \
    treeHolder.ui

RESOURCES += \
    images.qrc

