#include "tomeowindow.h"
#include "ui_mainwindow.h"


tomeoWindow::tomeoWindow(std::vector<TheButtonInfo> Inputvideos)
{
    ui = new Ui::MainWindow;
    ui->setupUi(this);
    videos = Inputvideos;
    videoWidget = new QVideoWidget;

    // the QMediaPlayer which controls the playback    
    player = new ThePlayer;
    player->setVideoOutput(videoWidget);



    QWidget *buttonWidget = new QWidget();
    // a list of the buttons
    // the buttons are arranged horizontally
    QHBoxLayout *layout = new QHBoxLayout();
    buttonLayout = layout;
    buttonWidget->setLayout(layout);

    QScrollArea *scroll = new QScrollArea();
    scroll->setWidget(buttonWidget);
    scroll->setWidgetResizable(true);
    scroll->setMinimumHeight(160);
    scroll->setMaximumHeight(180);

    makeFriends();
    makeTreeWidget();

    // tell the player what buttons and videos are available
    player->setContent(&buttons, & videos, ui);

    ui->playPause->connect( ui->playPause, SIGNAL(clicked()), player, SLOT(playPause()) ); // to change pic on button just create a new button class and connect this signal toa slot wich changes icon

    ui->horizontalSlider->setMinimum(0);
    ui->horizontalSlider->setMaximum(100);
    player->setNotifyInterval(1000);

    // top buttons
    ui->ViewFriends->connect( ui->ViewFriends, SIGNAL(clicked()), this, SLOT(OpenFriends()) ); // to change pic on button just create a new button class and connect this signal toa slot wich changes icon


    connect(player, SIGNAL(durationChanged(qint64)), this, SLOT(setMax(qint64)));

    connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(toInteger(qint64)));
    connect(this, SIGNAL(setIntPosition(int)), ui->horizontalSlider, SLOT(setValue(int)));

    connect(ui->horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(toQInt64(int)));
    connect(this, SIGNAL(setQInt64Position(qint64)), player, SLOT(setPosition(qint64)));

    connect(ui->addTimeline, SIGNAL(clicked()), this, SLOT(addToTimeline()));
    connect(ui->removeTimeline, SIGNAL(clicked()), this, SLOT(removeFromTimeline()));

    setWindowTitle("tomeo");
    setMinimumSize(800, 680);

    fillLocationTabs();
    fillActivityTabs();

    // add the video and the buttons to the top level widget
    ui->VidoeLayout->addWidget(videoWidget);
    ui->ThumbnailBox->addWidget(scroll);



}

void tomeoWindow::toInteger(qint64 value) {
    int intVal = (int) value / 1000;
    emit setIntPosition(intVal);
}

void tomeoWindow::toQInt64(int value) {
    qint64 qIntVal = (qint64) value * 1000;
    emit setQInt64Position(qIntVal);
}

void tomeoWindow::setMax(qint64 value) {
    ui->horizontalSlider->setMaximum((int) value / 1000);
}

void tomeoWindow::fillLocationTabs(){
    // England
    QWidget *EnglandButtonWidget = new QWidget();
    EnglandButtonWidget->setMaximumWidth(160);
    EnglandButtonWidget->setLayout(ui->EnglandV);
    for ( int i = 0; i < 3; i++ ) {
        TheButton *button = new TheButton(EnglandButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->EnglandV->addWidget(button);
        button->init(&videos.at(i));
        button->tab = "Location";
        button->page = "England";
    }
    ui->EnglandScroll->setWidget(EnglandButtonWidget);

    // China

    QWidget *ChinaButtonWidget = new QWidget();
    ChinaButtonWidget->setMaximumWidth(160);
    ChinaButtonWidget->setLayout(ui->ChinaV);
    for ( int i = 0; i < 3; i++ ) {
        TheButton *button = new TheButton(ChinaButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->ChinaV->addWidget(button);
        button->init(&videos.at((videos.size() - 2) - i));
        button->tab = "Location";
        button->page = "China";
    }
    ui->ChinaV->addStretch();
    ui->ChinaScroll->setWidget(ChinaButtonWidget);

    // Australia

    QWidget *AustraliaButtonWidget = new QWidget();
    AustraliaButtonWidget->setMaximumWidth(180);
    AustraliaButtonWidget->setLayout(ui->AustraliaV);
    for ( int i = 0; i < 1; i++ ) {
        TheButton *button = new TheButton(AustraliaButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->AustraliaV->addWidget(button);
        button->init(&videos.at(videos.size() - 1));
        button->tab = "Location";
        button->page = "Australia";
    }
    ui->AustraliaV->addStretch();
    ui->AustraliaScroll->setWidget(AustraliaButtonWidget);

}

void tomeoWindow::fillActivityTabs(){
    //Cycling

    QWidget *CycelingButtonWidget = new QWidget();
    CycelingButtonWidget->setMaximumWidth(180);
    CycelingButtonWidget->setLayout(ui->CycelingV);
    for ( int i = 0; i < 1; i++ ) {
        TheButton *button = new TheButton(CycelingButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->CycelingV->addWidget(button);
        button->init(&videos.at(0));
        button->tab = "Location";
        button->page = "Cycling";
    }
    ui->CycelingV->addStretch();
    ui->CyclingScroll->setWidget(CycelingButtonWidget);

    //Diving

    QWidget *DivingButtonWidget = new QWidget();
    DivingButtonWidget->setMaximumWidth(180);
    DivingButtonWidget->setLayout(ui->MiscV);
    for ( int i = 1; i < videos.size() - 1; i++ ) {
        TheButton *button = new TheButton(DivingButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->MiscV->addWidget(button);
        button->init(&videos.at(i));
        button->tab = "Location";
        button->page = "Diving";
    }
    ui->MiscV->addStretch();
    ui->MiscScroll->setWidget(DivingButtonWidget);

    // MISC

    QWidget *MiscButtonWidget = new QWidget();
    MiscButtonWidget->setMaximumWidth(180);
    MiscButtonWidget->setLayout(ui->DivingV);
    for ( int i = 0; i < 1; i++ ) {
        TheButton *button = new TheButton(MiscButtonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        TabButtons.push_back(button);
        ui->DivingV->addWidget(button);
        button->init(&videos.at(videos.size()-1));
        button->tab = "Location";
        button->page = "Diving";
    }
    ui->DivingV->addStretch();
    ui->DivingScroll->setWidget(MiscButtonWidget);

}

void tomeoWindow::OpenFriends(){
    ui->addTimeline->hide();
    ui->removeTimeline->hide();
    treeHolder->hide();
    videoWidget->hide();
    ui->playPause->hide();
    ui->horizontalSlider->hide();
    friendsW->show();
    for (int i = 0; i < int(Profiles.size()); i++) {
        Profiles.at(i)->hide();
    }

}

void tomeoWindow::makeFriends(){
    friendsW = new Friends();
    ui->VidoeLayout->addWidget(friendsW);
    friendsW->hide();
    //friendsW->setDisabled(true);
    ui->line->hide();

    std::vector<QString> usernames = {"TomSchofield","MohamedFakeih","LewisJackson","JoshuaArlott-Beal","JakeGalvin"};
//    std::vector<QString> images = {"C:/Users/schof/LeedsUni/year2/ui_cw3/images/gigachad.png"};
    std::vector<QString> images = {":/gigachad.png"};


    for ( int i = 0; i < 5; i++ ) {
        Profile *profile = new Profile(usernames[i], images[0]);
        ui->VidoeLayout->addWidget(profile);
        profile->hide();
        Profiles.push_back(profile);
        for ( int i = 0; i < 4; i++ ) {
            TheButton *button = new TheButton(profile);
            button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
            profile->addTheButton(button);
            //TabButtons.push_back(button);
            //ui->DivingV->addWidget(button);
            button->init(&videos.at(i));
        }

        ProfileCard *profileCard = new ProfileCard(friendsW, usernames[i],images[0]);
//        profileCard->connect(profileCard, SIGNAL(signalUserName(QString)), this, SLOT(openProfile(QString))); // when clicked, tell the player to play.
        profileCard->connect(profileCard, SIGNAL(signalUserName(QString)), this, SLOT(openProfile(QString))); // when clicked, tell the player to play.
        friendsW->ProfileCards.push_back(profileCard);
        friendsW->addProfileCards(profileCard);
    }

    player->friendsW = friendsW;
    player->Profiles = Profiles;
    player->videoWidget = videoWidget;

}

void tomeoWindow::openProfile(QString s){
    ui->addTimeline->hide();
    ui->removeTimeline->hide();
    treeHolder->hide();
    friendsW->hide();
    for ( int i = 0; i < int(Profiles.size()); i++ ) {
        QString check = Profiles.at(i)->username;
        if(check == s){
            ui->searchButton->setText(s);
            Profiles.at(i)->show();
//            Profiles[i]->show();
        }
    }
}

void tomeoWindow::makeTreeWidget(){
    treeHolder = new TreeHolder(videos);
    connect(treeHolder->play, SIGNAL(clicked()), this, SLOT(PlayTreeVid()));
    connect(ui->treeButton, SIGNAL(clicked()), this, SLOT(openTreeHolder()));
    ui->VidoeLayout->addWidget(treeHolder);
    treeHolder->hide();
    player->treeHolder = treeHolder;

}

void tomeoWindow::PlayTreeVid(){
//    QIcon* icon; // icon to display
//    TheButtonInfo* info = new TheButtonInfo(treeHolder->CurrentVid, icon);
//    player->jumpTo(info);
    player->setMedia( treeHolder->CurrentVid);
    for (int i = 0; i < videos.size()-1; i++) {
        if(*videos.at(i).url == treeHolder->CurrentVid){
            player->currentlyPlaying = &videos.at(i);
        }
    }
    player->play();
    player->counter =0;
    if(ui){
        ui->addTimeline->show();
        ui->removeTimeline->show();
        ui->playPause->show();
        ui->horizontalSlider->show();
        ui->playPause->setText("Pause");
        friendsW->hide();
        treeHolder->hide();
        videoWidget->show();

        for (int i = 0; i < int(Profiles.size()); i++) {
            Profiles.at(i)->hide();
        }
    }
}

void tomeoWindow::openTreeHolder(){
    ui->addTimeline->hide();
    ui->removeTimeline->hide();
    videoWidget->hide();
    ui->playPause->hide();
    ui->horizontalSlider->hide();
    friendsW->hide();
    for (int i = 0; i < int(Profiles.size()); i++) {
        Profiles.at(i)->hide();
    }
    treeHolder->show();
}

void tomeoWindow::addToTimeline() {
    if(player->currentlyPlaying){
        TheButton *button = new TheButton(buttonLayout -> parentWidget());
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo*))); // when clicked, tell the player to play.
        buttons.push_back(button);
        buttonLayout->addWidget(button);
        button->init(player -> getCurrent());
    }
}

void tomeoWindow::removeFromTimeline() {
    if (!buttons.empty()){
        TheButton* lastButton = buttons.back();
        buttonLayout->removeWidget(lastButton);
        delete lastButton;
        buttons.pop_back();
        }

}
