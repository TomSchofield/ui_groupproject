/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *LeftBox;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QTabWidget *LocationTabBox;
    QWidget *Location;
    QToolBox *toolBox;
    QWidget *page_3;
    QWidget *page_4;
    QWidget *page_7;
    QWidget *page_8;
    QWidget *page_9;
    QWidget *page_10;
    QWidget *Activity;
    QToolBox *toolBox_2;
    QWidget *page_5;
    QWidget *page_6;
    QWidget *page_11;
    QWidget *page_12;
    QWidget *page_13;
    QVBoxLayout *RightBox;
    QVBoxLayout *VideoLayout;
    QHBoxLayout *MediaControlsLayout;
    QPushButton *playPause;
    QSlider *horizontalSlider;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *addTimeline;
    QPushButton *removeTimeline;
    QVBoxLayout *ThumbnailBox;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(811, 569);
        MainWindow->setMinimumSize(QSize(0, 0));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LeftBox = new QVBoxLayout();
        LeftBox->setObjectName(QString::fromUtf8("LeftBox"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        LeftBox->addLayout(horizontalLayout_2);

        LocationTabBox = new QTabWidget(centralwidget);
        LocationTabBox->setObjectName(QString::fromUtf8("LocationTabBox"));
        Location = new QWidget();
        Location->setObjectName(QString::fromUtf8("Location"));
        toolBox = new QToolBox(Location);
        toolBox->setObjectName(QString::fromUtf8("toolBox"));
        toolBox->setGeometry(QRect(10, 10, 211, 411));
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        page_3->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_3, QString::fromUtf8("England"));
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        page_4->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_4, QString::fromUtf8("France"));
        page_7 = new QWidget();
        page_7->setObjectName(QString::fromUtf8("page_7"));
        page_7->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_7, QString::fromUtf8("Spain"));
        page_8 = new QWidget();
        page_8->setObjectName(QString::fromUtf8("page_8"));
        page_8->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_8, QString::fromUtf8("America "));
        page_9 = new QWidget();
        page_9->setObjectName(QString::fromUtf8("page_9"));
        page_9->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_9, QString::fromUtf8("India"));
        page_10 = new QWidget();
        page_10->setObjectName(QString::fromUtf8("page_10"));
        page_10->setGeometry(QRect(0, 0, 211, 231));
        toolBox->addItem(page_10, QString::fromUtf8("Australia"));
        LocationTabBox->addTab(Location, QString());
        Activity = new QWidget();
        Activity->setObjectName(QString::fromUtf8("Activity"));
        toolBox_2 = new QToolBox(Activity);
        toolBox_2->setObjectName(QString::fromUtf8("toolBox_2"));
        toolBox_2->setGeometry(QRect(10, 10, 211, 411));
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        page_5->setGeometry(QRect(0, 0, 100, 30));
        toolBox_2->addItem(page_5, QString::fromUtf8("Hiking"));
        page_6 = new QWidget();
        page_6->setObjectName(QString::fromUtf8("page_6"));
        page_6->setGeometry(QRect(0, 0, 211, 261));
        toolBox_2->addItem(page_6, QString::fromUtf8("Cycling"));
        page_11 = new QWidget();
        page_11->setObjectName(QString::fromUtf8("page_11"));
        page_11->setGeometry(QRect(0, 0, 100, 30));
        toolBox_2->addItem(page_11, QString::fromUtf8("Caving"));
        page_12 = new QWidget();
        page_12->setObjectName(QString::fromUtf8("page_12"));
        page_12->setGeometry(QRect(0, 0, 100, 30));
        toolBox_2->addItem(page_12, QString::fromUtf8("Climbing"));
        page_13 = new QWidget();
        page_13->setObjectName(QString::fromUtf8("page_13"));
        page_13->setGeometry(QRect(0, 0, 100, 30));
        toolBox_2->addItem(page_13, QString::fromUtf8("Sailing"));
        LocationTabBox->addTab(Activity, QString());

        LeftBox->addWidget(LocationTabBox);


        horizontalLayout->addLayout(LeftBox);

        RightBox = new QVBoxLayout();
        RightBox->setObjectName(QString::fromUtf8("RightBox"));
        VideoLayout = new QVBoxLayout();
        VideoLayout->setObjectName(QString::fromUtf8("VideoLayout"));

        RightBox->addLayout(VideoLayout);

        MediaControlsLayout = new QHBoxLayout();
        MediaControlsLayout->setObjectName(QString::fromUtf8("MediaControlsLayout"));
        playPause = new QPushButton(centralwidget);
        playPause->setObjectName(QString::fromUtf8("playPause"));
        playPause->setMinimumSize(QSize(100, 0));

        MediaControlsLayout->addWidget(playPause);

        horizontalSlider = new QSlider(centralwidget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        MediaControlsLayout->addWidget(horizontalSlider);


        RightBox->addLayout(MediaControlsLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        addTimeline = new QPushButton(centralwidget);
        addTimeline->setObjectName(QString::fromUtf8("addTimeline"));

        horizontalLayout_3->addWidget(addTimeline);

        removeTimeline = new QPushButton(centralwidget);
        removeTimeline->setObjectName(QString::fromUtf8("removeTimeline"));

        horizontalLayout_3->addWidget(removeTimeline);


        RightBox->addLayout(horizontalLayout_3);

        ThumbnailBox = new QVBoxLayout();
        ThumbnailBox->setObjectName(QString::fromUtf8("ThumbnailBox"));

        RightBox->addLayout(ThumbnailBox);


        horizontalLayout->addLayout(RightBox);

        horizontalLayout->setStretch(1, 1);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 811, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        LocationTabBox->setCurrentIndex(0);
        toolBox->setCurrentIndex(5);
        toolBox_2->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Search", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_3), QCoreApplication::translate("MainWindow", "England", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_4), QCoreApplication::translate("MainWindow", "France", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_7), QCoreApplication::translate("MainWindow", "Spain", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_8), QCoreApplication::translate("MainWindow", "America ", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_9), QCoreApplication::translate("MainWindow", "India", nullptr));
        toolBox->setItemText(toolBox->indexOf(page_10), QCoreApplication::translate("MainWindow", "Australia", nullptr));
        LocationTabBox->setTabText(LocationTabBox->indexOf(Location), QCoreApplication::translate("MainWindow", "Location", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_5), QCoreApplication::translate("MainWindow", "Hiking", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_6), QCoreApplication::translate("MainWindow", "Cycling", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_11), QCoreApplication::translate("MainWindow", "Caving", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_12), QCoreApplication::translate("MainWindow", "Climbing", nullptr));
        toolBox_2->setItemText(toolBox_2->indexOf(page_13), QCoreApplication::translate("MainWindow", "Sailing", nullptr));
        LocationTabBox->setTabText(LocationTabBox->indexOf(Activity), QCoreApplication::translate("MainWindow", "Activity", nullptr));
        playPause->setText(QCoreApplication::translate("MainWindow", "Pause", nullptr));
        addTimeline->setText(QCoreApplication::translate("MainWindow", "Add to Timeline", nullptr));
        removeTimeline->setText(QCoreApplication::translate("MainWindow", "Remove latest video from Timeline", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
