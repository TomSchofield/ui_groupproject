/********************************************************************************
** Form generated from reading UI file 'tree.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREE_H
#define UI_TREE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Tree
{
public:
    QLabel *label;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_13;
    QPushButton *pushButton_14;
    QPushButton *pushButton_15;
    QPushButton *pushButton_16;
    QPushButton *pushButton_17;
    QPushButton *pushButton_18;
    QPushButton *pushButton_19;
    QPushButton *pushButton_20;
    QPushButton *pushButton_21;
    QPushButton *pushButton_22;
    QPushButton *pushButton_23;
    QPushButton *pushButton_24;
    QPushButton *pushButton_25;
    QPushButton *pushButton_26;
    QPushButton *pushButton_27;
    QPushButton *pushButton_28;
    QPushButton *pushButton_29;
    QPushButton *pushButton_30;
    QPushButton *pushButton_31;
    QPushButton *pushButton_32;
    QPushButton *pushButton_33;
    QPushButton *pushButton_34;
    QPushButton *pushButton_35;
    QPushButton *pushButton_36;
    QPushButton *pushButton_37;
    QPushButton *pushButton_38;
    QPushButton *pushButton_39;
    QPushButton *pushButton_40;
    QPushButton *pushButton_41;
    QPushButton *pushButton_42;
    QPushButton *pushButton_43;
    QPushButton *pushButton_44;
    QPushButton *pushButton_45;
    QPushButton *pushButton_46;
    QPushButton *pushButton_47;
    QPushButton *pushButton_48;
    QPushButton *pushButton_49;
    QPushButton *pushButton_50;
    QPushButton *pushButton_51;
    QPushButton *pushButton_52;
    QPushButton *pushButton_53;
    QPushButton *pushButton_54;
    QPushButton *pushButton_55;
    QPushButton *pushButton_56;
    QPushButton *pushButton_57;
    QPushButton *pushButton_58;
    QPushButton *pushButton_59;
    QPushButton *pushButton_60;
    QPushButton *pushButton_61;
    QPushButton *pushButton_62;
    QPushButton *pushButton_63;
    QPushButton *pushButton_64;
    QPushButton *pushButton_65;
    QPushButton *pushButton_66;
    QPushButton *pushButton_67;
    QPushButton *pushButton_68;
    QPushButton *pushButton_69;
    QPushButton *pushButton_70;
    QPushButton *pushButton_71;
    QPushButton *pushButton_72;
    QPushButton *pushButton_73;
    QPushButton *pushButton_74;
    QPushButton *pushButton_75;
    QPushButton *pushButton_76;
    QPushButton *pushButton_77;
    QPushButton *pushButton_78;
    QPushButton *pushButton_79;
    QPushButton *pushButton_80;
    QPushButton *pushButton_81;
    QPushButton *pushButton_82;

    void setupUi(QWidget *Tree)
    {
        if (Tree->objectName().isEmpty())
            Tree->setObjectName(QString::fromUtf8("Tree"));
        Tree->resize(1271, 831);
        Tree->setMinimumSize(QSize(1271, 831));
        Tree->setMaximumSize(QSize(1271, 831));
        Tree->setAutoFillBackground(true);
        Tree->setStyleSheet(QString::fromUtf8(""));
        label = new QLabel(Tree);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 0, 1271, 831));
        label->setMinimumSize(QSize(1271, 831));
        label->setMaximumSize(QSize(1271, 831));
        label->setAutoFillBackground(false);
        label->setStyleSheet(QString::fromUtf8(""));
        label->setPixmap(QPixmap(QString::fromUtf8(":/Tree.png")));
        pushButton = new QPushButton(Tree);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(90, 60, 53, 31));
        pushButton_2 = new QPushButton(Tree);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(162, 110, 53, 31));
        pushButton_3 = new QPushButton(Tree);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(19, 12, 52, 31));
        pushButton_4 = new QPushButton(Tree);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(234, 157, 53, 31));
        pushButton_5 = new QPushButton(Tree);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(306, 203, 53, 31));
        pushButton_6 = new QPushButton(Tree);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(378, 251, 53, 31));
        pushButton_7 = new QPushButton(Tree);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(523, 350, 53, 31));
        pushButton_7->setStyleSheet(QString::fromUtf8(""));
        pushButton_8 = new QPushButton(Tree);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(450, 300, 53, 31));
        pushButton_9 = new QPushButton(Tree);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setGeometry(QRect(595, 396, 53, 31));
        pushButton_9->setStyleSheet(QString::fromUtf8("#pushButton_9:focus{\n"
"background-color: rgb(0, 170, 127);\n"
"}\n"
""));
        pushButton_10 = new QPushButton(Tree);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setGeometry(QRect(738, 492, 53, 31));
        pushButton_10->setStyleSheet(QString::fromUtf8("#pushButton_10:focus{\n"
"background-color: rgb(0, 170, 127);\n"
"}\n"
""));
        pushButton_11 = new QPushButton(Tree);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setGeometry(QRect(667, 444, 53, 31));
        pushButton_11->setStyleSheet(QString::fromUtf8("#pushButton_11:focus{\n"
"background-color: rgb(0, 170, 127);\n"
"}\n"
""));
        pushButton_12 = new QPushButton(Tree);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        pushButton_12->setGeometry(QRect(666, 540, 53, 31));
        pushButton_12->setStyleSheet(QString::fromUtf8(""));
        pushButton_13 = new QPushButton(Tree);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));
        pushButton_13->setGeometry(QRect(738, 590, 53, 31));
        pushButton_14 = new QPushButton(Tree);
        pushButton_14->setObjectName(QString::fromUtf8("pushButton_14"));
        pushButton_14->setGeometry(QRect(666, 637, 53, 31));
        pushButton_15 = new QPushButton(Tree);
        pushButton_15->setObjectName(QString::fromUtf8("pushButton_15"));
        pushButton_15->setGeometry(QRect(738, 683, 53, 31));
        pushButton_16 = new QPushButton(Tree);
        pushButton_16->setObjectName(QString::fromUtf8("pushButton_16"));
        pushButton_16->setGeometry(QRect(666, 733, 53, 31));
        pushButton_17 = new QPushButton(Tree);
        pushButton_17->setObjectName(QString::fromUtf8("pushButton_17"));
        pushButton_17->setGeometry(QRect(810, 730, 53, 31));
        pushButton_18 = new QPushButton(Tree);
        pushButton_18->setObjectName(QString::fromUtf8("pushButton_18"));
        pushButton_18->setGeometry(QRect(882, 780, 53, 31));
        pushButton_19 = new QPushButton(Tree);
        pushButton_19->setObjectName(QString::fromUtf8("pushButton_19"));
        pushButton_19->setGeometry(QRect(882, 683, 53, 31));
        pushButton_20 = new QPushButton(Tree);
        pushButton_20->setObjectName(QString::fromUtf8("pushButton_20"));
        pushButton_20->setGeometry(QRect(955, 635, 53, 31));
        pushButton_21 = new QPushButton(Tree);
        pushButton_21->setObjectName(QString::fromUtf8("pushButton_21"));
        pushButton_21->setGeometry(QRect(1098, 637, 53, 31));
        pushButton_22 = new QPushButton(Tree);
        pushButton_22->setObjectName(QString::fromUtf8("pushButton_22"));
        pushButton_22->setGeometry(QRect(1026, 490, 53, 31));
        pushButton_23 = new QPushButton(Tree);
        pushButton_23->setObjectName(QString::fromUtf8("pushButton_23"));
        pushButton_23->setGeometry(QRect(1026, 588, 53, 31));
        pushButton_24 = new QPushButton(Tree);
        pushButton_24->setObjectName(QString::fromUtf8("pushButton_24"));
        pushButton_24->setGeometry(QRect(1099, 540, 53, 31));
        pushButton_25 = new QPushButton(Tree);
        pushButton_25->setObjectName(QString::fromUtf8("pushButton_25"));
        pushButton_25->setGeometry(QRect(954, 540, 53, 31));
        pushButton_26 = new QPushButton(Tree);
        pushButton_26->setObjectName(QString::fromUtf8("pushButton_26"));
        pushButton_26->setGeometry(QRect(881, 492, 53, 31));
        pushButton_27 = new QPushButton(Tree);
        pushButton_27->setObjectName(QString::fromUtf8("pushButton_27"));
        pushButton_27->setGeometry(QRect(810, 444, 53, 31));
        pushButton_27->setStyleSheet(QString::fromUtf8(""));
        pushButton_28 = new QPushButton(Tree);
        pushButton_28->setObjectName(QString::fromUtf8("pushButton_28"));
        pushButton_28->setGeometry(QRect(954, 444, 53, 31));
        pushButton_29 = new QPushButton(Tree);
        pushButton_29->setObjectName(QString::fromUtf8("pushButton_29"));
        pushButton_29->setGeometry(QRect(522, 443, 53, 31));
        pushButton_29->setStyleSheet(QString::fromUtf8(""));
        pushButton_30 = new QPushButton(Tree);
        pushButton_30->setObjectName(QString::fromUtf8("pushButton_30"));
        pushButton_30->setGeometry(QRect(450, 490, 53, 31));
        pushButton_31 = new QPushButton(Tree);
        pushButton_31->setObjectName(QString::fromUtf8("pushButton_31"));
        pushButton_31->setGeometry(QRect(522, 540, 53, 31));
        pushButton_32 = new QPushButton(Tree);
        pushButton_32->setObjectName(QString::fromUtf8("pushButton_32"));
        pushButton_32->setGeometry(QRect(523, 638, 53, 31));
        pushButton_33 = new QPushButton(Tree);
        pushButton_33->setObjectName(QString::fromUtf8("pushButton_33"));
        pushButton_33->setGeometry(QRect(450, 590, 53, 31));
        pushButton_34 = new QPushButton(Tree);
        pushButton_34->setObjectName(QString::fromUtf8("pushButton_34"));
        pushButton_34->setGeometry(QRect(450, 684, 53, 31));
        pushButton_35 = new QPushButton(Tree);
        pushButton_35->setObjectName(QString::fromUtf8("pushButton_35"));
        pushButton_35->setGeometry(QRect(450, 780, 53, 31));
        pushButton_36 = new QPushButton(Tree);
        pushButton_36->setObjectName(QString::fromUtf8("pushButton_36"));
        pushButton_36->setGeometry(QRect(379, 540, 53, 31));
        pushButton_37 = new QPushButton(Tree);
        pushButton_37->setObjectName(QString::fromUtf8("pushButton_37"));
        pushButton_37->setGeometry(QRect(378, 443, 53, 31));
        pushButton_38 = new QPushButton(Tree);
        pushButton_38->setObjectName(QString::fromUtf8("pushButton_38"));
        pushButton_38->setGeometry(QRect(378, 637, 53, 31));
        pushButton_39 = new QPushButton(Tree);
        pushButton_39->setObjectName(QString::fromUtf8("pushButton_39"));
        pushButton_39->setGeometry(QRect(306, 588, 53, 31));
        pushButton_40 = new QPushButton(Tree);
        pushButton_40->setObjectName(QString::fromUtf8("pushButton_40"));
        pushButton_40->setGeometry(QRect(235, 637, 53, 31));
        pushButton_41 = new QPushButton(Tree);
        pushButton_41->setObjectName(QString::fromUtf8("pushButton_41"));
        pushButton_41->setGeometry(QRect(378, 732, 53, 31));
        pushButton_42 = new QPushButton(Tree);
        pushButton_42->setObjectName(QString::fromUtf8("pushButton_42"));
        pushButton_42->setGeometry(QRect(306, 781, 53, 31));
        pushButton_43 = new QPushButton(Tree);
        pushButton_43->setObjectName(QString::fromUtf8("pushButton_43"));
        pushButton_43->setGeometry(QRect(234, 732, 53, 31));
        pushButton_44 = new QPushButton(Tree);
        pushButton_44->setObjectName(QString::fromUtf8("pushButton_44"));
        pushButton_44->setGeometry(QRect(163, 780, 53, 31));
        pushButton_45 = new QPushButton(Tree);
        pushButton_45->setObjectName(QString::fromUtf8("pushButton_45"));
        pushButton_45->setGeometry(QRect(162, 683, 53, 31));
        pushButton_46 = new QPushButton(Tree);
        pushButton_46->setObjectName(QString::fromUtf8("pushButton_46"));
        pushButton_46->setGeometry(QRect(18, 683, 53, 31));
        pushButton_47 = new QPushButton(Tree);
        pushButton_47->setObjectName(QString::fromUtf8("pushButton_47"));
        pushButton_47->setGeometry(QRect(90, 638, 53, 31));
        pushButton_48 = new QPushButton(Tree);
        pushButton_48->setObjectName(QString::fromUtf8("pushButton_48"));
        pushButton_48->setGeometry(QRect(18, 589, 53, 31));
        pushButton_49 = new QPushButton(Tree);
        pushButton_49->setObjectName(QString::fromUtf8("pushButton_49"));
        pushButton_49->setGeometry(QRect(162, 589, 53, 31));
        pushButton_50 = new QPushButton(Tree);
        pushButton_50->setObjectName(QString::fromUtf8("pushButton_50"));
        pushButton_50->setGeometry(QRect(235, 540, 53, 31));
        pushButton_51 = new QPushButton(Tree);
        pushButton_51->setObjectName(QString::fromUtf8("pushButton_51"));
        pushButton_51->setGeometry(QRect(307, 492, 53, 31));
        pushButton_52 = new QPushButton(Tree);
        pushButton_52->setObjectName(QString::fromUtf8("pushButton_52"));
        pushButton_52->setGeometry(QRect(162, 491, 53, 31));
        pushButton_53 = new QPushButton(Tree);
        pushButton_53->setObjectName(QString::fromUtf8("pushButton_53"));
        pushButton_53->setGeometry(QRect(90, 444, 53, 31));
        pushButton_54 = new QPushButton(Tree);
        pushButton_54->setObjectName(QString::fromUtf8("pushButton_54"));
        pushButton_54->setGeometry(QRect(90, 540, 53, 31));
        pushButton_55 = new QPushButton(Tree);
        pushButton_55->setObjectName(QString::fromUtf8("pushButton_55"));
        pushButton_55->setGeometry(QRect(378, 349, 53, 31));
        pushButton_56 = new QPushButton(Tree);
        pushButton_56->setObjectName(QString::fromUtf8("pushButton_56"));
        pushButton_56->setGeometry(QRect(234, 253, 53, 31));
        pushButton_57 = new QPushButton(Tree);
        pushButton_57->setObjectName(QString::fromUtf8("pushButton_57"));
        pushButton_57->setGeometry(QRect(163, 300, 53, 31));
        pushButton_58 = new QPushButton(Tree);
        pushButton_58->setObjectName(QString::fromUtf8("pushButton_58"));
        pushButton_58->setGeometry(QRect(163, 205, 53, 31));
        pushButton_59 = new QPushButton(Tree);
        pushButton_59->setObjectName(QString::fromUtf8("pushButton_59"));
        pushButton_59->setGeometry(QRect(90, 250, 53, 31));
        pushButton_60 = new QPushButton(Tree);
        pushButton_60->setObjectName(QString::fromUtf8("pushButton_60"));
        pushButton_60->setGeometry(QRect(90, 350, 53, 31));
        pushButton_61 = new QPushButton(Tree);
        pushButton_61->setObjectName(QString::fromUtf8("pushButton_61"));
        pushButton_61->setGeometry(QRect(18, 300, 53, 31));
        pushButton_62 = new QPushButton(Tree);
        pushButton_62->setObjectName(QString::fromUtf8("pushButton_62"));
        pushButton_62->setGeometry(QRect(18, 203, 53, 31));
        pushButton_63 = new QPushButton(Tree);
        pushButton_63->setObjectName(QString::fromUtf8("pushButton_63"));
        pushButton_63->setGeometry(QRect(18, 110, 53, 31));
        pushButton_64 = new QPushButton(Tree);
        pushButton_64->setObjectName(QString::fromUtf8("pushButton_64"));
        pushButton_64->setGeometry(QRect(162, 12, 53, 31));
        pushButton_65 = new QPushButton(Tree);
        pushButton_65->setObjectName(QString::fromUtf8("pushButton_65"));
        pushButton_65->setGeometry(QRect(306, 108, 53, 31));
        pushButton_66 = new QPushButton(Tree);
        pushButton_66->setObjectName(QString::fromUtf8("pushButton_66"));
        pushButton_66->setGeometry(QRect(307, 11, 53, 31));
        pushButton_67 = new QPushButton(Tree);
        pushButton_67->setObjectName(QString::fromUtf8("pushButton_67"));
        pushButton_67->setGeometry(QRect(378, 155, 53, 31));
        pushButton_68 = new QPushButton(Tree);
        pushButton_68->setObjectName(QString::fromUtf8("pushButton_68"));
        pushButton_68->setGeometry(QRect(377, 60, 53, 31));
        pushButton_69 = new QPushButton(Tree);
        pushButton_69->setObjectName(QString::fromUtf8("pushButton_69"));
        pushButton_69->setGeometry(QRect(450, 10, 53, 31));
        pushButton_70 = new QPushButton(Tree);
        pushButton_70->setObjectName(QString::fromUtf8("pushButton_70"));
        pushButton_70->setGeometry(QRect(522, 252, 53, 31));
        pushButton_71 = new QPushButton(Tree);
        pushButton_71->setObjectName(QString::fromUtf8("pushButton_71"));
        pushButton_71->setGeometry(QRect(666, 348, 53, 31));
        pushButton_71->setStyleSheet(QString::fromUtf8(""));
        pushButton_72 = new QPushButton(Tree);
        pushButton_72->setObjectName(QString::fromUtf8("pushButton_72"));
        pushButton_72->setGeometry(QRect(738, 300, 53, 31));
        pushButton_73 = new QPushButton(Tree);
        pushButton_73->setObjectName(QString::fromUtf8("pushButton_73"));
        pushButton_73->setGeometry(QRect(666, 251, 53, 31));
        pushButton_74 = new QPushButton(Tree);
        pushButton_74->setObjectName(QString::fromUtf8("pushButton_74"));
        pushButton_74->setGeometry(QRect(739, 205, 53, 31));
        pushButton_75 = new QPushButton(Tree);
        pushButton_75->setObjectName(QString::fromUtf8("pushButton_75"));
        pushButton_75->setGeometry(QRect(666, 156, 53, 31));
        pushButton_76 = new QPushButton(Tree);
        pushButton_76->setObjectName(QString::fromUtf8("pushButton_76"));
        pushButton_76->setGeometry(QRect(882, 204, 53, 31));
        pushButton_77 = new QPushButton(Tree);
        pushButton_77->setObjectName(QString::fromUtf8("pushButton_77"));
        pushButton_77->setGeometry(QRect(810, 156, 53, 31));
        pushButton_78 = new QPushButton(Tree);
        pushButton_78->setObjectName(QString::fromUtf8("pushButton_78"));
        pushButton_78->setGeometry(QRect(738, 12, 53, 31));
        pushButton_79 = new QPushButton(Tree);
        pushButton_79->setObjectName(QString::fromUtf8("pushButton_79"));
        pushButton_79->setGeometry(QRect(738, 108, 53, 31));
        pushButton_80 = new QPushButton(Tree);
        pushButton_80->setObjectName(QString::fromUtf8("pushButton_80"));
        pushButton_80->setGeometry(QRect(666, 60, 53, 31));
        pushButton_81 = new QPushButton(Tree);
        pushButton_81->setObjectName(QString::fromUtf8("pushButton_81"));
        pushButton_81->setGeometry(QRect(954, 157, 53, 31));
        pushButton_82 = new QPushButton(Tree);
        pushButton_82->setObjectName(QString::fromUtf8("pushButton_82"));
        pushButton_82->setGeometry(QRect(882, 107, 53, 31));

        retranslateUi(Tree);

        QMetaObject::connectSlotsByName(Tree);
    } // setupUi

    void retranslateUi(QWidget *Tree)
    {
        Tree->setWindowTitle(QCoreApplication::translate("Tree", "Form", nullptr));
        label->setText(QString());
        pushButton->setText(QString());
        pushButton_2->setText(QString());
        pushButton_3->setText(QString());
        pushButton_4->setText(QString());
        pushButton_5->setText(QString());
        pushButton_6->setText(QString());
        pushButton_7->setText(QString());
        pushButton_8->setText(QString());
        pushButton_9->setText(QString());
        pushButton_10->setText(QString());
        pushButton_11->setText(QString());
        pushButton_12->setText(QString());
        pushButton_13->setText(QString());
        pushButton_14->setText(QString());
        pushButton_15->setText(QString());
        pushButton_16->setText(QString());
        pushButton_17->setText(QString());
        pushButton_18->setText(QString());
        pushButton_19->setText(QString());
        pushButton_20->setText(QString());
        pushButton_21->setText(QString());
        pushButton_22->setText(QString());
        pushButton_23->setText(QString());
        pushButton_24->setText(QString());
        pushButton_25->setText(QString());
        pushButton_26->setText(QString());
        pushButton_27->setText(QString());
        pushButton_28->setText(QString());
        pushButton_29->setText(QString());
        pushButton_30->setText(QString());
        pushButton_31->setText(QString());
        pushButton_32->setText(QString());
        pushButton_33->setText(QString());
        pushButton_34->setText(QString());
        pushButton_35->setText(QString());
        pushButton_36->setText(QString());
        pushButton_37->setText(QString());
        pushButton_38->setText(QString());
        pushButton_39->setText(QString());
        pushButton_40->setText(QString());
        pushButton_41->setText(QString());
        pushButton_42->setText(QString());
        pushButton_43->setText(QString());
        pushButton_44->setText(QString());
        pushButton_45->setText(QString());
        pushButton_46->setText(QString());
        pushButton_47->setText(QString());
        pushButton_48->setText(QString());
        pushButton_49->setText(QString());
        pushButton_50->setText(QString());
        pushButton_51->setText(QString());
        pushButton_52->setText(QString());
        pushButton_53->setText(QString());
        pushButton_54->setText(QString());
        pushButton_55->setText(QString());
        pushButton_56->setText(QString());
        pushButton_57->setText(QString());
        pushButton_58->setText(QString());
        pushButton_59->setText(QString());
        pushButton_60->setText(QString());
        pushButton_61->setText(QString());
        pushButton_62->setText(QString());
        pushButton_63->setText(QString());
        pushButton_64->setText(QString());
        pushButton_65->setText(QString());
        pushButton_66->setText(QString());
        pushButton_67->setText(QString());
        pushButton_68->setText(QString());
        pushButton_69->setText(QString());
        pushButton_70->setText(QString());
        pushButton_71->setText(QString());
        pushButton_72->setText(QString());
        pushButton_73->setText(QString());
        pushButton_74->setText(QString());
        pushButton_75->setText(QString());
        pushButton_76->setText(QString());
        pushButton_77->setText(QString());
        pushButton_78->setText(QString());
        pushButton_79->setText(QString());
        pushButton_80->setText(QString());
        pushButton_81->setText(QString());
        pushButton_82->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Tree: public Ui_Tree {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREE_H
