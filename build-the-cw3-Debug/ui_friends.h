/********************************************************************************
** Form generated from reading UI file 'friends.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRIENDS_H
#define UI_FRIENDS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Friends
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *MainLayout;
    QLabel *TopText;
    QFrame *line_2;
    QHBoxLayout *FriendGrid;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *Friends)
    {
        if (Friends->objectName().isEmpty())
            Friends->setObjectName(QString::fromUtf8("Friends"));
        Friends->resize(613, 447);
        verticalLayout = new QVBoxLayout(Friends);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        MainLayout = new QVBoxLayout();
        MainLayout->setObjectName(QString::fromUtf8("MainLayout"));
        TopText = new QLabel(Friends);
        TopText->setObjectName(QString::fromUtf8("TopText"));
        TopText->setMinimumSize(QSize(0, 50));

        MainLayout->addWidget(TopText);

        line_2 = new QFrame(Friends);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        MainLayout->addWidget(line_2);

        FriendGrid = new QHBoxLayout();
        FriendGrid->setObjectName(QString::fromUtf8("FriendGrid"));

        MainLayout->addLayout(FriendGrid);


        verticalLayout->addLayout(MainLayout);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        retranslateUi(Friends);

        QMetaObject::connectSlotsByName(Friends);
    } // setupUi

    void retranslateUi(QWidget *Friends)
    {
        Friends->setWindowTitle(QCoreApplication::translate("Friends", "Form", nullptr));
        TopText->setText(QCoreApplication::translate("Friends", "Friends", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Friends: public Ui_Friends {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRIENDS_H
