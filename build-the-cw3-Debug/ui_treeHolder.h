/********************************************************************************
** Form generated from reading UI file 'treeHolder.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TREEHOLDER_H
#define UI_TREEHOLDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TreeHolder
{
public:
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *mainLayot;
    QVBoxLayout *MapLayout;
    QScrollArea *MapScroll;
    QWidget *scrollAreaWidgetContents;
    QFrame *line;
    QVBoxLayout *ButtonsLayout;

    void setupUi(QWidget *TreeHolder)
    {
        if (TreeHolder->objectName().isEmpty())
            TreeHolder->setObjectName(QString::fromUtf8("TreeHolder"));
        TreeHolder->resize(425, 316);
        horizontalLayout = new QHBoxLayout(TreeHolder);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        mainLayot = new QHBoxLayout();
        mainLayot->setObjectName(QString::fromUtf8("mainLayot"));
        MapLayout = new QVBoxLayout();
        MapLayout->setObjectName(QString::fromUtf8("MapLayout"));
        MapScroll = new QScrollArea(TreeHolder);
        MapScroll->setObjectName(QString::fromUtf8("MapScroll"));
        MapScroll->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 384, 292));
        MapScroll->setWidget(scrollAreaWidgetContents);

        MapLayout->addWidget(MapScroll);


        mainLayot->addLayout(MapLayout);

        line = new QFrame(TreeHolder);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        mainLayot->addWidget(line);

        ButtonsLayout = new QVBoxLayout();
        ButtonsLayout->setObjectName(QString::fromUtf8("ButtonsLayout"));

        mainLayot->addLayout(ButtonsLayout);

        mainLayot->setStretch(0, 1);

        horizontalLayout->addLayout(mainLayot);


        retranslateUi(TreeHolder);

        QMetaObject::connectSlotsByName(TreeHolder);
    } // setupUi

    void retranslateUi(QWidget *TreeHolder)
    {
        TreeHolder->setWindowTitle(QCoreApplication::translate("TreeHolder", "Form", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TreeHolder: public Ui_TreeHolder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TREEHOLDER_H
