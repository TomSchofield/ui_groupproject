/********************************************************************************
** Form generated from reading UI file 'profile.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFILE_H
#define UI_PROFILE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Profile
{
public:
    QVBoxLayout *verticalLayout;
    QVBoxLayout *base;
    QHBoxLayout *ProfileTopLayout;
    QLabel *ImageLable;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *LableLayout;
    QSpacerItem *verticalSpacer;
    QLabel *TopText;
    QSpacerItem *horizontalSpacer_2;
    QFrame *line;
    QHBoxLayout *VidLayout;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QWidget *Profile)
    {
        if (Profile->objectName().isEmpty())
            Profile->setObjectName(QString::fromUtf8("Profile"));
        Profile->resize(585, 472);
        verticalLayout = new QVBoxLayout(Profile);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        base = new QVBoxLayout();
        base->setObjectName(QString::fromUtf8("base"));
        ProfileTopLayout = new QHBoxLayout();
        ProfileTopLayout->setObjectName(QString::fromUtf8("ProfileTopLayout"));
        ImageLable = new QLabel(Profile);
        ImageLable->setObjectName(QString::fromUtf8("ImageLable"));
        ImageLable->setMinimumSize(QSize(100, 100));
        ImageLable->setMaximumSize(QSize(200, 200));
        ImageLable->setScaledContents(true);

        ProfileTopLayout->addWidget(ImageLable);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ProfileTopLayout->addItem(horizontalSpacer);

        LableLayout = new QVBoxLayout();
        LableLayout->setObjectName(QString::fromUtf8("LableLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        LableLayout->addItem(verticalSpacer);

        TopText = new QLabel(Profile);
        TopText->setObjectName(QString::fromUtf8("TopText"));
        TopText->setMinimumSize(QSize(0, 50));
        TopText->setLayoutDirection(Qt::LeftToRight);
        TopText->setStyleSheet(QString::fromUtf8("font: 16pt \"Arial\";"));
        TopText->setMidLineWidth(0);

        LableLayout->addWidget(TopText);


        ProfileTopLayout->addLayout(LableLayout);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ProfileTopLayout->addItem(horizontalSpacer_2);


        base->addLayout(ProfileTopLayout);

        line = new QFrame(Profile);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        base->addWidget(line);

        VidLayout = new QHBoxLayout();
        VidLayout->setObjectName(QString::fromUtf8("VidLayout"));

        base->addLayout(VidLayout);


        verticalLayout->addLayout(base);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);


        retranslateUi(Profile);

        QMetaObject::connectSlotsByName(Profile);
    } // setupUi

    void retranslateUi(QWidget *Profile)
    {
        Profile->setWindowTitle(QCoreApplication::translate("Profile", "Form", nullptr));
        ImageLable->setText(QString());
        TopText->setText(QCoreApplication::translate("Profile", "lable", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Profile: public Ui_Profile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFILE_H
