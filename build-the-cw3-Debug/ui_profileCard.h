/********************************************************************************
** Form generated from reading UI file 'profileCard.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROFILECARD_H
#define UI_PROFILECARD_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProfileCard
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *ImageLable;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *Username;
    QSpacerItem *horizontalSpacer_2;

    void setupUi(QWidget *ProfileCard)
    {
        if (ProfileCard->objectName().isEmpty())
            ProfileCard->setObjectName(QString::fromUtf8("ProfileCard"));
        ProfileCard->resize(120, 180);
        ProfileCard->setMinimumSize(QSize(120, 180));
        ProfileCard->setMaximumSize(QSize(200, 200));
        verticalLayout_2 = new QVBoxLayout(ProfileCard);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        ImageLable = new QLabel(ProfileCard);
        ImageLable->setObjectName(QString::fromUtf8("ImageLable"));
        ImageLable->setScaledContents(true);

        verticalLayout->addWidget(ImageLable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        Username = new QPushButton(ProfileCard);
        Username->setObjectName(QString::fromUtf8("Username"));
        Username->setMinimumSize(QSize(75, 20));
        Username->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        horizontalLayout->addWidget(Username);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addLayout(horizontalLayout);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(ProfileCard);

        QMetaObject::connectSlotsByName(ProfileCard);
    } // setupUi

    void retranslateUi(QWidget *ProfileCard)
    {
        ProfileCard->setWindowTitle(QCoreApplication::translate("ProfileCard", "Form", nullptr));
        ImageLable->setText(QString());
        Username->setText(QCoreApplication::translate("ProfileCard", "Username", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProfileCard: public Ui_ProfileCard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROFILECARD_H
