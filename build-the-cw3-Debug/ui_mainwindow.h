/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *LeftBox;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *searchButton;
    QTabWidget *LocationTabBox;
    QWidget *Location;
    QToolBox *LocationToolBox;
    QWidget *England;
    QScrollArea *EnglandScroll;
    QWidget *scrollAreaWidgetContents;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *EnglandV;
    QWidget *China;
    QScrollArea *ChinaScroll;
    QWidget *scrollAreaWidgetContents_2;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *ChinaV;
    QWidget *Spain;
    QScrollArea *SpainScroll;
    QWidget *scrollAreaWidgetContents_3;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *SpainV;
    QWidget *America;
    QScrollArea *AmericaScroll;
    QWidget *scrollAreaWidgetContents_4;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *AmericaV;
    QWidget *India;
    QScrollArea *IndiaScroll;
    QWidget *scrollAreaWidgetContents_5;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *IndiaV;
    QWidget *Australia;
    QScrollArea *AustraliaScroll;
    QWidget *scrollAreaWidgetContents_6;
    QWidget *verticalLayoutWidget_6;
    QVBoxLayout *AustraliaV;
    QWidget *Activity;
    QToolBox *ActivityToolBox;
    QWidget *Hiking;
    QScrollArea *HikingScroll;
    QWidget *scrollAreaWidgetContents_7;
    QWidget *verticalLayoutWidget_7;
    QVBoxLayout *HikingV;
    QWidget *Cycling;
    QScrollArea *CyclingScroll;
    QWidget *scrollAreaWidgetContents_8;
    QWidget *verticalLayoutWidget_8;
    QVBoxLayout *CycelingV;
    QWidget *Diving;
    QScrollArea *DivingScroll;
    QWidget *scrollAreaWidgetContents_9;
    QWidget *verticalLayoutWidget_9;
    QVBoxLayout *DivingV;
    QWidget *Climbing;
    QScrollArea *ClimbingScroll;
    QWidget *scrollAreaWidgetContents_10;
    QWidget *verticalLayoutWidget_10;
    QVBoxLayout *ClimbingV;
    QWidget *Misc;
    QScrollArea *MiscScroll;
    QWidget *scrollAreaWidgetContents_11;
    QWidget *verticalLayoutWidget_11;
    QVBoxLayout *MiscV;
    QFrame *line_2;
    QVBoxLayout *RightBox;
    QHBoxLayout *TopButtonLayout;
    QPushButton *ViewFriends;
    QSpacerItem *horizontalSpacer;
    QPushButton *treeButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *AddVid;
    QFrame *line;
    QVBoxLayout *VidoeLayout;
    QHBoxLayout *MediaControlsLayout;
    QPushButton *playPause;
    QSlider *horizontalSlider;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *addTimeline;
    QPushButton *removeTimeline;
    QVBoxLayout *ThumbnailBox;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(811, 585);
        MainWindow->setMinimumSize(QSize(0, 0));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        LeftBox = new QVBoxLayout();
        LeftBox->setObjectName(QString::fromUtf8("LeftBox"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        lineEdit = new QLineEdit(centralwidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        searchButton = new QPushButton(centralwidget);
        searchButton->setObjectName(QString::fromUtf8("searchButton"));
        searchButton->setMinimumSize(QSize(50, 20));
        searchButton->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        horizontalLayout_2->addWidget(searchButton);


        LeftBox->addLayout(horizontalLayout_2);

        LocationTabBox = new QTabWidget(centralwidget);
        LocationTabBox->setObjectName(QString::fromUtf8("LocationTabBox"));
        LocationTabBox->setMinimumSize(QSize(216, 477));
        LocationTabBox->setStyleSheet(QString::fromUtf8(""));
        Location = new QWidget();
        Location->setObjectName(QString::fromUtf8("Location"));
        LocationToolBox = new QToolBox(Location);
        LocationToolBox->setObjectName(QString::fromUtf8("LocationToolBox"));
        LocationToolBox->setGeometry(QRect(10, 10, 191, 411));
        England = new QWidget();
        England->setObjectName(QString::fromUtf8("England"));
        England->setGeometry(QRect(0, 0, 98, 28));
        EnglandScroll = new QScrollArea(England);
        EnglandScroll->setObjectName(QString::fromUtf8("EnglandScroll"));
        EnglandScroll->setGeometry(QRect(0, 0, 191, 241));
        EnglandScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        EnglandScroll->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget = new QWidget(scrollAreaWidgetContents);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(10, 10, 171, 221));
        EnglandV = new QVBoxLayout(verticalLayoutWidget);
        EnglandV->setObjectName(QString::fromUtf8("EnglandV"));
        EnglandV->setContentsMargins(0, 0, 0, 0);
        EnglandScroll->setWidget(scrollAreaWidgetContents);
        LocationToolBox->addItem(England, QString::fromUtf8("England"));
        China = new QWidget();
        China->setObjectName(QString::fromUtf8("China"));
        China->setGeometry(QRect(0, 0, 98, 28));
        ChinaScroll = new QScrollArea(China);
        ChinaScroll->setObjectName(QString::fromUtf8("ChinaScroll"));
        ChinaScroll->setGeometry(QRect(0, 0, 191, 241));
        ChinaScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        ChinaScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget_2 = new QWidget(scrollAreaWidgetContents_2);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(10, 10, 171, 221));
        ChinaV = new QVBoxLayout(verticalLayoutWidget_2);
        ChinaV->setObjectName(QString::fromUtf8("ChinaV"));
        ChinaV->setContentsMargins(0, 0, 0, 0);
        ChinaScroll->setWidget(scrollAreaWidgetContents_2);
        LocationToolBox->addItem(China, QString::fromUtf8("China"));
        Spain = new QWidget();
        Spain->setObjectName(QString::fromUtf8("Spain"));
        Spain->setGeometry(QRect(0, 0, 98, 28));
        SpainScroll = new QScrollArea(Spain);
        SpainScroll->setObjectName(QString::fromUtf8("SpainScroll"));
        SpainScroll->setGeometry(QRect(0, 0, 191, 241));
        SpainScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        SpainScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget_3 = new QWidget(scrollAreaWidgetContents_3);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(10, 10, 171, 221));
        SpainV = new QVBoxLayout(verticalLayoutWidget_3);
        SpainV->setObjectName(QString::fromUtf8("SpainV"));
        SpainV->setContentsMargins(0, 0, 0, 0);
        SpainScroll->setWidget(scrollAreaWidgetContents_3);
        LocationToolBox->addItem(Spain, QString::fromUtf8("Spain"));
        America = new QWidget();
        America->setObjectName(QString::fromUtf8("America"));
        America->setGeometry(QRect(0, 0, 98, 28));
        AmericaScroll = new QScrollArea(America);
        AmericaScroll->setObjectName(QString::fromUtf8("AmericaScroll"));
        AmericaScroll->setGeometry(QRect(0, 0, 191, 241));
        AmericaScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        AmericaScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_4 = new QWidget();
        scrollAreaWidgetContents_4->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_4"));
        scrollAreaWidgetContents_4->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget_4 = new QWidget(scrollAreaWidgetContents_4);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 10, 171, 221));
        AmericaV = new QVBoxLayout(verticalLayoutWidget_4);
        AmericaV->setObjectName(QString::fromUtf8("AmericaV"));
        AmericaV->setContentsMargins(0, 0, 0, 0);
        AmericaScroll->setWidget(scrollAreaWidgetContents_4);
        LocationToolBox->addItem(America, QString::fromUtf8("America "));
        India = new QWidget();
        India->setObjectName(QString::fromUtf8("India"));
        India->setGeometry(QRect(0, 0, 98, 28));
        IndiaScroll = new QScrollArea(India);
        IndiaScroll->setObjectName(QString::fromUtf8("IndiaScroll"));
        IndiaScroll->setGeometry(QRect(0, 0, 191, 241));
        IndiaScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        IndiaScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_5 = new QWidget();
        scrollAreaWidgetContents_5->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_5"));
        scrollAreaWidgetContents_5->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget_5 = new QWidget(scrollAreaWidgetContents_5);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 10, 171, 221));
        IndiaV = new QVBoxLayout(verticalLayoutWidget_5);
        IndiaV->setObjectName(QString::fromUtf8("IndiaV"));
        IndiaV->setContentsMargins(0, 0, 0, 0);
        IndiaScroll->setWidget(scrollAreaWidgetContents_5);
        LocationToolBox->addItem(India, QString::fromUtf8("India"));
        Australia = new QWidget();
        Australia->setObjectName(QString::fromUtf8("Australia"));
        Australia->setGeometry(QRect(0, 0, 191, 249));
        AustraliaScroll = new QScrollArea(Australia);
        AustraliaScroll->setObjectName(QString::fromUtf8("AustraliaScroll"));
        AustraliaScroll->setGeometry(QRect(0, 0, 191, 241));
        AustraliaScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        AustraliaScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_6 = new QWidget();
        scrollAreaWidgetContents_6->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_6"));
        scrollAreaWidgetContents_6->setGeometry(QRect(0, 0, 189, 239));
        verticalLayoutWidget_6 = new QWidget(scrollAreaWidgetContents_6);
        verticalLayoutWidget_6->setObjectName(QString::fromUtf8("verticalLayoutWidget_6"));
        verticalLayoutWidget_6->setGeometry(QRect(10, 10, 171, 221));
        AustraliaV = new QVBoxLayout(verticalLayoutWidget_6);
        AustraliaV->setObjectName(QString::fromUtf8("AustraliaV"));
        AustraliaV->setContentsMargins(0, 0, 0, 0);
        AustraliaScroll->setWidget(scrollAreaWidgetContents_6);
        LocationToolBox->addItem(Australia, QString::fromUtf8("Australia"));
        LocationTabBox->addTab(Location, QString());
        Activity = new QWidget();
        Activity->setObjectName(QString::fromUtf8("Activity"));
        ActivityToolBox = new QToolBox(Activity);
        ActivityToolBox->setObjectName(QString::fromUtf8("ActivityToolBox"));
        ActivityToolBox->setGeometry(QRect(10, 10, 191, 411));
        Hiking = new QWidget();
        Hiking->setObjectName(QString::fromUtf8("Hiking"));
        Hiking->setGeometry(QRect(0, 0, 191, 276));
        HikingScroll = new QScrollArea(Hiking);
        HikingScroll->setObjectName(QString::fromUtf8("HikingScroll"));
        HikingScroll->setGeometry(QRect(0, 0, 191, 271));
        HikingScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        HikingScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_7 = new QWidget();
        scrollAreaWidgetContents_7->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_7"));
        scrollAreaWidgetContents_7->setGeometry(QRect(0, 0, 189, 269));
        verticalLayoutWidget_7 = new QWidget(scrollAreaWidgetContents_7);
        verticalLayoutWidget_7->setObjectName(QString::fromUtf8("verticalLayoutWidget_7"));
        verticalLayoutWidget_7->setGeometry(QRect(10, 10, 171, 251));
        HikingV = new QVBoxLayout(verticalLayoutWidget_7);
        HikingV->setObjectName(QString::fromUtf8("HikingV"));
        HikingV->setContentsMargins(0, 0, 0, 0);
        HikingScroll->setWidget(scrollAreaWidgetContents_7);
        ActivityToolBox->addItem(Hiking, QString::fromUtf8("Hiking"));
        Cycling = new QWidget();
        Cycling->setObjectName(QString::fromUtf8("Cycling"));
        Cycling->setGeometry(QRect(0, 0, 98, 28));
        CyclingScroll = new QScrollArea(Cycling);
        CyclingScroll->setObjectName(QString::fromUtf8("CyclingScroll"));
        CyclingScroll->setGeometry(QRect(0, 0, 191, 271));
        CyclingScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        CyclingScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_8 = new QWidget();
        scrollAreaWidgetContents_8->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_8"));
        scrollAreaWidgetContents_8->setGeometry(QRect(0, 0, 189, 269));
        verticalLayoutWidget_8 = new QWidget(scrollAreaWidgetContents_8);
        verticalLayoutWidget_8->setObjectName(QString::fromUtf8("verticalLayoutWidget_8"));
        verticalLayoutWidget_8->setGeometry(QRect(10, 10, 171, 251));
        CycelingV = new QVBoxLayout(verticalLayoutWidget_8);
        CycelingV->setObjectName(QString::fromUtf8("CycelingV"));
        CycelingV->setContentsMargins(0, 0, 0, 0);
        CyclingScroll->setWidget(scrollAreaWidgetContents_8);
        ActivityToolBox->addItem(Cycling, QString::fromUtf8("Cycling"));
        Diving = new QWidget();
        Diving->setObjectName(QString::fromUtf8("Diving"));
        Diving->setGeometry(QRect(0, 0, 98, 28));
        DivingScroll = new QScrollArea(Diving);
        DivingScroll->setObjectName(QString::fromUtf8("DivingScroll"));
        DivingScroll->setGeometry(QRect(0, 0, 191, 271));
        DivingScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        DivingScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_9 = new QWidget();
        scrollAreaWidgetContents_9->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_9"));
        scrollAreaWidgetContents_9->setGeometry(QRect(0, 0, 189, 269));
        verticalLayoutWidget_9 = new QWidget(scrollAreaWidgetContents_9);
        verticalLayoutWidget_9->setObjectName(QString::fromUtf8("verticalLayoutWidget_9"));
        verticalLayoutWidget_9->setGeometry(QRect(10, 10, 171, 251));
        DivingV = new QVBoxLayout(verticalLayoutWidget_9);
        DivingV->setObjectName(QString::fromUtf8("DivingV"));
        DivingV->setContentsMargins(0, 0, 0, 0);
        DivingScroll->setWidget(scrollAreaWidgetContents_9);
        ActivityToolBox->addItem(Diving, QString::fromUtf8("Diving"));
        Climbing = new QWidget();
        Climbing->setObjectName(QString::fromUtf8("Climbing"));
        Climbing->setGeometry(QRect(0, 0, 98, 28));
        ClimbingScroll = new QScrollArea(Climbing);
        ClimbingScroll->setObjectName(QString::fromUtf8("ClimbingScroll"));
        ClimbingScroll->setGeometry(QRect(0, 0, 191, 271));
        ClimbingScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        ClimbingScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_10 = new QWidget();
        scrollAreaWidgetContents_10->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_10"));
        scrollAreaWidgetContents_10->setGeometry(QRect(0, 0, 189, 269));
        verticalLayoutWidget_10 = new QWidget(scrollAreaWidgetContents_10);
        verticalLayoutWidget_10->setObjectName(QString::fromUtf8("verticalLayoutWidget_10"));
        verticalLayoutWidget_10->setGeometry(QRect(10, 10, 171, 251));
        ClimbingV = new QVBoxLayout(verticalLayoutWidget_10);
        ClimbingV->setObjectName(QString::fromUtf8("ClimbingV"));
        ClimbingV->setContentsMargins(0, 0, 0, 0);
        ClimbingScroll->setWidget(scrollAreaWidgetContents_10);
        ActivityToolBox->addItem(Climbing, QString::fromUtf8("Climbing"));
        Misc = new QWidget();
        Misc->setObjectName(QString::fromUtf8("Misc"));
        Misc->setGeometry(QRect(0, 0, 98, 28));
        MiscScroll = new QScrollArea(Misc);
        MiscScroll->setObjectName(QString::fromUtf8("MiscScroll"));
        MiscScroll->setGeometry(QRect(0, 0, 191, 271));
        MiscScroll->setStyleSheet(QString::fromUtf8("background-color: #3cbaa2; border: 1px solid black;\n"
"border-radius: 2px;"));
        MiscScroll->setWidgetResizable(true);
        scrollAreaWidgetContents_11 = new QWidget();
        scrollAreaWidgetContents_11->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_11"));
        scrollAreaWidgetContents_11->setGeometry(QRect(0, 0, 189, 269));
        verticalLayoutWidget_11 = new QWidget(scrollAreaWidgetContents_11);
        verticalLayoutWidget_11->setObjectName(QString::fromUtf8("verticalLayoutWidget_11"));
        verticalLayoutWidget_11->setGeometry(QRect(10, 10, 171, 251));
        MiscV = new QVBoxLayout(verticalLayoutWidget_11);
        MiscV->setObjectName(QString::fromUtf8("MiscV"));
        MiscV->setContentsMargins(0, 0, 0, 0);
        MiscScroll->setWidget(scrollAreaWidgetContents_11);
        ActivityToolBox->addItem(Misc, QString::fromUtf8("Misc"));
        LocationTabBox->addTab(Activity, QString());

        LeftBox->addWidget(LocationTabBox);


        horizontalLayout->addLayout(LeftBox);

        line_2 = new QFrame(centralwidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line_2);

        RightBox = new QVBoxLayout();
        RightBox->setObjectName(QString::fromUtf8("RightBox"));
        TopButtonLayout = new QHBoxLayout();
        TopButtonLayout->setObjectName(QString::fromUtf8("TopButtonLayout"));
        ViewFriends = new QPushButton(centralwidget);
        ViewFriends->setObjectName(QString::fromUtf8("ViewFriends"));
        ViewFriends->setMinimumSize(QSize(75, 20));
        ViewFriends->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        TopButtonLayout->addWidget(ViewFriends);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        TopButtonLayout->addItem(horizontalSpacer);

        treeButton = new QPushButton(centralwidget);
        treeButton->setObjectName(QString::fromUtf8("treeButton"));
        treeButton->setMinimumSize(QSize(75, 20));
        treeButton->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        TopButtonLayout->addWidget(treeButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        TopButtonLayout->addItem(horizontalSpacer_2);

        AddVid = new QPushButton(centralwidget);
        AddVid->setObjectName(QString::fromUtf8("AddVid"));
        AddVid->setMinimumSize(QSize(75, 20));
        AddVid->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        TopButtonLayout->addWidget(AddVid);


        RightBox->addLayout(TopButtonLayout);

        line = new QFrame(centralwidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        RightBox->addWidget(line);

        VidoeLayout = new QVBoxLayout();
        VidoeLayout->setObjectName(QString::fromUtf8("VidoeLayout"));

        RightBox->addLayout(VidoeLayout);

        MediaControlsLayout = new QHBoxLayout();
        MediaControlsLayout->setObjectName(QString::fromUtf8("MediaControlsLayout"));
        playPause = new QPushButton(centralwidget);
        playPause->setObjectName(QString::fromUtf8("playPause"));
        playPause->setMinimumSize(QSize(100, 20));
        playPause->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        MediaControlsLayout->addWidget(playPause);

        horizontalSlider = new QSlider(centralwidget);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        MediaControlsLayout->addWidget(horizontalSlider);


        RightBox->addLayout(MediaControlsLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        addTimeline = new QPushButton(centralwidget);
        addTimeline->setObjectName(QString::fromUtf8("addTimeline"));
        addTimeline->setMinimumSize(QSize(75, 20));
        addTimeline->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        horizontalLayout_5->addWidget(addTimeline);

        removeTimeline = new QPushButton(centralwidget);
        removeTimeline->setObjectName(QString::fromUtf8("removeTimeline"));
        removeTimeline->setMinimumSize(QSize(75, 20));
        removeTimeline->setStyleSheet(QString::fromUtf8("QPushButton:pressed {\n"
"    background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,   stop:0 rgba(60, 186, 162, 255), stop:1 rgba(98, 211, 162, 255))\n"
"}\n"
"QPushButton {\n"
"     background-color: #3cbaa2; border: 1px solid black;\n"
"     border-radius: 2px;\n"
"}\n"
""));

        horizontalLayout_5->addWidget(removeTimeline);


        RightBox->addLayout(horizontalLayout_5);

        ThumbnailBox = new QVBoxLayout();
        ThumbnailBox->setObjectName(QString::fromUtf8("ThumbnailBox"));

        RightBox->addLayout(ThumbnailBox);

        RightBox->setStretch(2, 1);

        horizontalLayout->addLayout(RightBox);

        horizontalLayout->setStretch(2, 1);
        MainWindow->setCentralWidget(centralwidget);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        LocationTabBox->setCurrentIndex(1);
        LocationToolBox->setCurrentIndex(5);
        ActivityToolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        searchButton->setText(QCoreApplication::translate("MainWindow", "Search", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(England), QCoreApplication::translate("MainWindow", "England", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(China), QCoreApplication::translate("MainWindow", "China", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(Spain), QCoreApplication::translate("MainWindow", "Spain", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(America), QCoreApplication::translate("MainWindow", "America ", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(India), QCoreApplication::translate("MainWindow", "India", nullptr));
        LocationToolBox->setItemText(LocationToolBox->indexOf(Australia), QCoreApplication::translate("MainWindow", "Australia", nullptr));
        LocationTabBox->setTabText(LocationTabBox->indexOf(Location), QCoreApplication::translate("MainWindow", "Location", nullptr));
        ActivityToolBox->setItemText(ActivityToolBox->indexOf(Hiking), QCoreApplication::translate("MainWindow", "Hiking", nullptr));
        ActivityToolBox->setItemText(ActivityToolBox->indexOf(Cycling), QCoreApplication::translate("MainWindow", "Cycling", nullptr));
        ActivityToolBox->setItemText(ActivityToolBox->indexOf(Diving), QCoreApplication::translate("MainWindow", "Diving", nullptr));
        ActivityToolBox->setItemText(ActivityToolBox->indexOf(Climbing), QCoreApplication::translate("MainWindow", "Climbing", nullptr));
        ActivityToolBox->setItemText(ActivityToolBox->indexOf(Misc), QCoreApplication::translate("MainWindow", "Misc", nullptr));
        LocationTabBox->setTabText(LocationTabBox->indexOf(Activity), QCoreApplication::translate("MainWindow", "Activity", nullptr));
        ViewFriends->setText(QCoreApplication::translate("MainWindow", "Friends", nullptr));
        treeButton->setText(QCoreApplication::translate("MainWindow", "Tree View", nullptr));
        AddVid->setText(QCoreApplication::translate("MainWindow", "Add Video", nullptr));
        playPause->setText(QCoreApplication::translate("MainWindow", "Pause", nullptr));
        addTimeline->setText(QCoreApplication::translate("MainWindow", "Add to Timeline", nullptr));
        removeTimeline->setText(QCoreApplication::translate("MainWindow", "Remove latest video from Timeline", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
