/****************************************************************************
** Meta object code from reading C++ file 'tomeowindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../the/tomeowindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tomeowindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_tomeoWindow_t {
    QByteArrayData data[14];
    char stringdata0[158];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_tomeoWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_tomeoWindow_t qt_meta_stringdata_tomeoWindow = {
    {
QT_MOC_LITERAL(0, 0, 11), // "tomeoWindow"
QT_MOC_LITERAL(1, 12, 14), // "setIntPosition"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 17), // "setQInt64Position"
QT_MOC_LITERAL(4, 46, 8), // "toQInt64"
QT_MOC_LITERAL(5, 55, 9), // "toInteger"
QT_MOC_LITERAL(6, 65, 6), // "setMax"
QT_MOC_LITERAL(7, 72, 11), // "OpenFriends"
QT_MOC_LITERAL(8, 84, 11), // "openProfile"
QT_MOC_LITERAL(9, 96, 1), // "s"
QT_MOC_LITERAL(10, 98, 14), // "openTreeHolder"
QT_MOC_LITERAL(11, 113, 11), // "PlayTreeVid"
QT_MOC_LITERAL(12, 125, 18), // "removeFromTimeline"
QT_MOC_LITERAL(13, 144, 13) // "addToTimeline"

    },
    "tomeoWindow\0setIntPosition\0\0"
    "setQInt64Position\0toQInt64\0toInteger\0"
    "setMax\0OpenFriends\0openProfile\0s\0"
    "openTreeHolder\0PlayTreeVid\0"
    "removeFromTimeline\0addToTimeline"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_tomeoWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       3,    1,   72,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   75,    2, 0x08 /* Private */,
       5,    1,   78,    2, 0x08 /* Private */,
       6,    1,   81,    2, 0x08 /* Private */,
       7,    0,   84,    2, 0x08 /* Private */,
       8,    1,   85,    2, 0x08 /* Private */,
      10,    0,   88,    2, 0x08 /* Private */,
      11,    0,   89,    2, 0x08 /* Private */,
      12,    0,   90,    2, 0x08 /* Private */,
      13,    0,   91,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::LongLong,    2,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void, QMetaType::LongLong,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void tomeoWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<tomeoWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->setIntPosition((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setQInt64Position((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 2: _t->toQInt64((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->toInteger((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 4: _t->setMax((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 5: _t->OpenFriends(); break;
        case 6: _t->openProfile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->openTreeHolder(); break;
        case 8: _t->PlayTreeVid(); break;
        case 9: _t->removeFromTimeline(); break;
        case 10: _t->addToTimeline(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (tomeoWindow::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&tomeoWindow::setIntPosition)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (tomeoWindow::*)(qint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&tomeoWindow::setQInt64Position)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject tomeoWindow::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_tomeoWindow.data,
    qt_meta_data_tomeoWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *tomeoWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *tomeoWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_tomeoWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int tomeoWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void tomeoWindow::setIntPosition(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void tomeoWindow::setQInt64Position(qint64 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
