/********************************************************************************
** Form generated from reading UI file 'player.ui'
**
** Created by: Qt User Interface Compiler version 5.13.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYER_H
#define UI_PLAYER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *VidoeLayout;
    QHBoxLayout *MediaControlsLayout;
    QPushButton *playPause;
    QSlider *horizontalSlider;
    QVBoxLayout *ThumbnailBox;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(605, 483);
        horizontalLayout_3 = new QHBoxLayout(Form);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        VidoeLayout = new QVBoxLayout();
        VidoeLayout->setObjectName(QString::fromUtf8("VidoeLayout"));

        verticalLayout->addLayout(VidoeLayout);

        MediaControlsLayout = new QHBoxLayout();
        MediaControlsLayout->setObjectName(QString::fromUtf8("MediaControlsLayout"));
        playPause = new QPushButton(Form);
        playPause->setObjectName(QString::fromUtf8("playPause"));
        playPause->setMinimumSize(QSize(100, 0));

        MediaControlsLayout->addWidget(playPause);

        horizontalSlider = new QSlider(Form);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setOrientation(Qt::Horizontal);

        MediaControlsLayout->addWidget(horizontalSlider);


        verticalLayout->addLayout(MediaControlsLayout);

        ThumbnailBox = new QVBoxLayout();
        ThumbnailBox->setObjectName(QString::fromUtf8("ThumbnailBox"));

        verticalLayout->addLayout(ThumbnailBox);


        horizontalLayout_3->addLayout(verticalLayout);


        retranslateUi(Form);

        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QCoreApplication::translate("Form", "Form", nullptr));
        playPause->setText(QCoreApplication::translate("Form", "PushButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYER_H
